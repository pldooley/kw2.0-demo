export const environment = {
    production: true,
    hmr       : false,
    firebase  : {
        apiKey: "AIzaSyBKy4NPoB27hWq0lQniYNROggc9fOawSKw",
        authDomain: "teppen-49917.firebaseapp.com",
        databaseURL: "https://teppen-49917.firebaseio.com",
        projectId: "teppen-49917",
        storageBucket: "teppen-49917.appspot.com",
        messagingSenderId: "97228466358"
    }
};
